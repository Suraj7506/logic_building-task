//  ---------------First Method------------------------

const numbers1 = [1, 2, 3, 2, 4, 5, 5, 6];

const set = new Set(numbers1);

const duplicates = numbers1.filter(item => {
    if (set.has(item)) {
        set.delete(item);
    } else {
        return item;
    }
});

console.log(duplicates);


// --------------second Method------------------

// const a = [4,3,6,3,4,3]

// function count_duplicate(a){
//  let counts = {}

//  for(let i =0; i < a.length; i++){ 
//      if (counts[a[i]]){
//      counts[a[i]] += 1
//      } else {
//      counts[a[i]] = 1
//      }
//     }  
//     for (let prop in counts){
//         if (counts[prop] >= 2){
//             console.log(prop + " counted: " + counts[prop] + " times.")
//         }
//     }
//   console.log(counts)
// }

// count_duplicate(a)