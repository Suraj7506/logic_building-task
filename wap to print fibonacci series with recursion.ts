let fibrec = function(val){
    if (val == 1){
        return [0,1]
    }
    else{
        let Newval = fibrec(val-1)
        Newval?.push(Newval[Newval.length - 1] + Newval?.[Newval.length-2]);
        return Newval;
    }
};

console.log(fibrec(5));