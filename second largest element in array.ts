function second_highest(arr)
{
  var second_highest = arr.sort(function(a, b) { return b - a; })[1];
  return second_highest;
}

console.log(second_highest([12, 35, 1, 10, 34, 1, 35]));