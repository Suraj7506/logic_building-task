let fibo = num => {
    let a = '1';
    let b = '0';
    let temp = '';

    while(num>0){
        temp = a;
        a = a + b;
        b = temp;
        num--;
    }
    return b;
}

console.log(fibo(8));

// function fibonacci(num)
//     {   
//         if(num==1)
//             return 0;
//         if (num == 2)
//             return 1;
//         return fibonacci(num - 1) + fibonacci(num - 2);
//     }
// console.log("Fibonacci(5): "+fibonacci(5)+"<br>");
// console.log("Fibonacci(8): "+fibonacci(8)+"<br>");