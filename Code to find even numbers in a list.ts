const numbers = [7, 10, 15, 8, 13, 18, 6];

const evens = numbers.filter((num) => num % 2 === 0);

// [10, 8, 18, 6]
console.log(evens);